import os
import tempfile
from collections import OrderedDict
from typing import Any, Callable, List, Dict, Tuple, Optional

import pytest
import yaml

from leetconfig.definitions.server_client import (
    ServerClientConfig,
    ServerClientConfigDefinition,
)
from leetconfig.format.file_yaml import YAMLConfigFormat
from leetconfig.group import ConfigGroup
from leetconfig.parser import ConfigParser
from leetconfig_test.unit.format import ConfigEntryTestCase, TestFormat


class YAMLConfigEntryTestCase(ConfigEntryTestCase):
    def __init__(
        self,
        test_name,  # type: str
        entry_name,  # type: Tuple[str, str]
        definition_flags,  # type: Dict[str, Any]
        expected_value,  # type: Any
        value,  # type: Any
    ):
        super(YAMLConfigEntryTestCase, self).__init__(
            test_name, entry_name, definition_flags, expected_value
        )
        self.value = value


NAMESPACES = ["namespace_1", None, "namespace_2"]
INTEGER_TEST_CASE = YAMLConfigEntryTestCase(
    "Integer",
    ("test_integer", "ti"),
    dict(),
    1,
    1,
)
FLOAT_TEST_CASE = YAMLConfigEntryTestCase(
    "Float",
    ("test_float", "tf"),
    dict(),
    1.0,
    1.0,
)
STRING_TEST_CASE = YAMLConfigEntryTestCase(
    "String",
    ("test_string", "ts"),
    dict(),
    "string",
    "string",
)
BOOLEAN_TEST_CASE = YAMLConfigEntryTestCase(
    "Boolean",
    ("test_bool", "tb"),
    dict(),
    True,
    True,
)
LIST_TEST_CASE = YAMLConfigEntryTestCase(
    "List",
    ("test_list", "tl"),
    dict(is_list=True),
    [1, 2],
    [1, 2],
)
DICT_TEST_CASE = YAMLConfigEntryTestCase(
    "Dict",
    ("test_dict", "td"),
    dict(is_dict=True),
    OrderedDict([("key1", 1), ("key2", 1), ("key3", 2)]),
    OrderedDict([("key1", 1), ("key2", 1), ("key3", 2)]),
)
TEST_CASES = [
    INTEGER_TEST_CASE,
    FLOAT_TEST_CASE,
    STRING_TEST_CASE,
    LIST_TEST_CASE,
    DICT_TEST_CASE,
]


class ConfigDefinitionSerializationTestCase(object):
    def __init__(
        self,
        config_definition_ctor,  # type: Callable[[], ConfigGroup]
        config,  # type: Any
    ):
        self.config_definition_ctor = config_definition_ctor
        self.config = config


class TestYamlFormat:
    @staticmethod
    def _create_config(
        test_cases,  # type: List[YAMLConfigEntryTestCase]
        nested_names,  # type: List[Optional[str]]
    ):
        # type: (...)  -> Dict[str, Any]
        config: Dict[str, Any] = dict()
        nested_config = config
        for nested_name in nested_names:
            if nested_name is None:
                continue
            nested_config[nested_name] = dict()
            nested_config = nested_config[nested_name]
        for test_case in test_cases:
            nested_config[test_case.entry_name[0]] = test_case.value
        return config

    @staticmethod
    def _create_config_file(
        config,  # type: Dict[str, Any]
    ):
        # type: (...) -> str
        file_handle, file_path = tempfile.mkstemp()
        os.close(file_handle)
        with open(file_path, "wb") as f:
            data = yaml.dump(
                config, Dumper=yaml.Dumper, default_flow_style=False
            ).encode("utf-8")
            f.write(data)
        return file_path

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_value(self, test_case):
        config_path = TestYamlFormat._create_config_file(
            TestYamlFormat._create_config([test_case], [])
        )
        config_format = YAMLConfigFormat(config_path)
        TestFormat.validate_deserialization(config_format, [test_case], [])

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_nested_value(self, test_case):
        config_path = TestYamlFormat._create_config_file(
            TestYamlFormat._create_config([test_case], NAMESPACES)
        )
        config_format = YAMLConfigFormat(config_path)
        TestFormat.validate_deserialization(config_format, [test_case], NAMESPACES)

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_value(self, test_case):
        config_yaml = yaml.dump(
            TestYamlFormat._create_config([test_case], []),
            Dumper=yaml.Dumper,
            default_flow_style=False,
        )
        config_format = YAMLConfigFormat([])
        TestFormat.validate_serialization(config_format, [test_case], [], config_yaml)

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_nested_value(self, test_case):
        config_yaml = yaml.dump(
            TestYamlFormat._create_config([test_case], NAMESPACES),
            Dumper=yaml.Dumper,
            default_flow_style=False,
        )
        config_format = YAMLConfigFormat([])
        TestFormat.validate_serialization(
            config_format, [test_case], NAMESPACES, config_yaml
        )

    def test_deserialize_values(self):
        config_path = TestYamlFormat._create_config_file(
            TestYamlFormat._create_config(TEST_CASES, [])
        )
        config_format = YAMLConfigFormat(config_path)
        TestFormat.validate_deserialization(config_format, TEST_CASES, [])

    def test_deserialize_nested_values(self):
        config_path = TestYamlFormat._create_config_file(
            TestYamlFormat._create_config(TEST_CASES, NAMESPACES)
        )
        config_format = YAMLConfigFormat(config_path)
        TestFormat.validate_deserialization(config_format, TEST_CASES, NAMESPACES)

    def test_serialize_values(self):
        config_yaml = yaml.dump(
            TestYamlFormat._create_config(TEST_CASES, []),
            Dumper=yaml.Dumper,
            default_flow_style=False,
        )
        config_format = YAMLConfigFormat([])
        TestFormat.validate_serialization(config_format, TEST_CASES, [], config_yaml)

    def test_serialize_nested_values(self):
        config_yaml = yaml.dump(
            TestYamlFormat._create_config(TEST_CASES, NAMESPACES),
            Dumper=yaml.Dumper,
            default_flow_style=False,
        )
        config_format = YAMLConfigFormat([])
        TestFormat.validate_serialization(
            config_format, TEST_CASES, NAMESPACES, config_yaml
        )

    @pytest.mark.parametrize(
        "test_case",
        [
            ConfigDefinitionSerializationTestCase(
                lambda: ServerClientConfigDefinition("generic_server"),
                ServerClientConfig("hostname", 1337, 0),
            )
        ],
    )
    def test_serialize_config_group_or_namespace(
        self,
        test_case,  # type: ConfigDefinitionSerializationTestCase
        tmp_path,
    ):
        # type: (...) -> None
        """
        Seraliaze and deserialize a ConfigNamespace.

        Assert deserialized namespace equals original
        """
        # Write config group or namespace to yml file
        path = tmp_path / "config.yml"
        yaml_config_format = YAMLConfigFormat([str(path)])
        config_definition = test_case.config_definition_ctor()
        config_definition.populate(test_case.config)  # type: ignore
        dumped_config = yaml_config_format.serialize(config_definition).encode("utf-8")
        path.write_bytes(dumped_config)

        # Extract config group or namepaces from yml file
        extracted_config_definition = test_case.config_definition_ctor()
        config_parser = ConfigParser(
            "test_parser",
            "test config parser",
            groups=[extracted_config_definition],
            sources=[yaml_config_format],
            force_no_cli=True,
        )
        config_parser.extract()
        extracted_config = extracted_config_definition.export()  # type: ignore

        # Assert equal
        assert test_case.config == extracted_config
