from collections import OrderedDict

import pytest
from typing import List, Tuple, Callable, Dict, Any

from leetconfig.format.cli import CLIConfigFormat
from leetconfig_test.unit.format import ConfigEntryTestCase, TestFormat


class CLIConfigEntryTestCase(ConfigEntryTestCase):
    def __init__(
        self,
        test_name,  # type: str
        entry_name,  # type: Tuple[str, str]
        definition_flags,  # type: Dict[str, Any]
        expected_value,  # type: Any
        command_flags,  # type: Tuple[List[str], List[str]]
        ns_command_flags,  # type: Tuple[List[str], List[str]]
    ):
        super(CLIConfigEntryTestCase, self).__init__(
            test_name, entry_name, definition_flags, expected_value
        )
        self.command_flags = command_flags
        self.ns_command_flags = ns_command_flags


NAMESPACES = ["namespace_1", None, "namespace_2"]
SIMPLE_TEST_CASE = CLIConfigEntryTestCase(
    "Simple",
    ("test_integer", "ti"),
    dict(),
    "1",
    (["--test-integer", "1"], ["-ti", "1"]),
    (["--namespace-1-namespace-2-test-integer", "1"], ["-n1n2ti", "1"]),
)
FLAG_BOOLEAN_TEST_CASE = CLIConfigEntryTestCase(
    "Flag",
    ("test_flag", "tf"),
    dict(is_flag=True),
    True,
    (["--test-flag"], ["-tf"]),
    (["--namespace-1-namespace-2-test-flag"], ["-n1n2tf"]),
)
LIST_TEST_CASE = CLIConfigEntryTestCase(
    "List",
    ("test_list", "tl"),
    dict(is_list=True),
    ["1", "2"],
    (["--test-list", "1", "2"], ["-tl", "1", "2"]),
    (["--namespace-1-namespace-2-test-list", "1", "2"], ["-n1n2tl", "1", "2"]),
)
POSITIONAL_TEST_CASE = CLIConfigEntryTestCase(
    "Positional",
    ("test_positional", "tp"),
    dict(is_positional=True),
    "1",
    (["1"], ["1"]),
    (["1"], ["1"]),
)
DICT_TEST_CASE = CLIConfigEntryTestCase(
    "Dict",
    ("test_dict", "td"),
    dict(is_dict=True),
    OrderedDict([("key1", "1"), ("key2", "2")]),
    (["--test-dict", "key1:1", "key2:2"], ["-td", "key1:1", "key2:2"]),
    (
        ["--namespace-1-namespace-2-test-dict", "key1:1", "key2:2"],
        ["-n1n2td", "key1:1", "key2:2"],
    ),
)
TEST_CASES = [
    SIMPLE_TEST_CASE,
    FLAG_BOOLEAN_TEST_CASE,
    LIST_TEST_CASE,
    DICT_TEST_CASE,
    POSITIONAL_TEST_CASE,
]


class TestCLI:
    @staticmethod
    def _build_command(
        test_cases,  # type: List[CLIConfigEntryTestCase]
        get_flags,  # type: Callable[[CLIConfigEntryTestCase], List[str]]
    ):
        # type: (...) -> List[str]
        command = []
        is_nargs = False
        for test_case in test_cases:
            if test_case.definition_flags.get("is_positional") and is_nargs is True:
                command.append("--")
            command.extend(get_flags(test_case))
            is_nargs = (
                test_case.definition_flags.get("is_list") is True
                or test_case.definition_flags.get("is_dict") is True
            )
        return command

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.command_flags[0])
        TestFormat.validate_deserialization(config_format, [test_case], [])

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_short_flag_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.command_flags[1])
        TestFormat.validate_deserialization(config_format, [test_case], [])

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_namespaced_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.ns_command_flags[0])
        TestFormat.validate_deserialization(config_format, [test_case], NAMESPACES)

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_deserialize_namespaced_short_flag_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.ns_command_flags[1])
        TestFormat.validate_deserialization(config_format, [test_case], NAMESPACES)

    def test_deserialize_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.command_flags[0])
        config_format = CLIConfigFormat("test", command)
        TestFormat.validate_deserialization(config_format, TEST_CASES, [])

    def test_deserialize_short_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.command_flags[1])
        config_format = CLIConfigFormat("test", command)
        TestFormat.validate_deserialization(config_format, TEST_CASES, [])

    def test_deserialize_namespaced_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.ns_command_flags[0])
        config_format = CLIConfigFormat("test", command)
        TestFormat.validate_deserialization(config_format, TEST_CASES, NAMESPACES)

    def test_deserialize_namespaced_short_flag_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.ns_command_flags[1])
        config_format = CLIConfigFormat("test", command)
        TestFormat.validate_deserialization(config_format, TEST_CASES, NAMESPACES)

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_value(self, test_case):
        config_format = CLIConfigFormat("test", [], False)
        TestFormat.validate_serialization(
            config_format, [test_case], [], test_case.command_flags[0]
        )

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_short_flag_value(self, test_case):
        config_format = CLIConfigFormat("test", [], True)
        TestFormat.validate_serialization(
            config_format, [test_case], [], test_case.command_flags[1]
        )

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_namespaced_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.ns_command_flags[0])
        TestFormat.validate_serialization(
            config_format, [test_case], NAMESPACES, test_case.ns_command_flags[0]
        )

    @pytest.mark.parametrize("test_case", TEST_CASES, ids=lambda t: t.test_name)
    def test_serialize_namespaced_short_flag_value(self, test_case):
        config_format = CLIConfigFormat("test", test_case.ns_command_flags[1])
        TestFormat.validate_serialization(
            config_format, [test_case], NAMESPACES, test_case.ns_command_flags[0]
        )

    def test_serialize_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.command_flags[0])
        config_format = CLIConfigFormat("test", [], False)
        TestFormat.validate_serialization(
            config_format,
            TEST_CASES,
            [],
            command,
        )

    def test_serialize_short_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.command_flags[1])
        config_format = CLIConfigFormat("test", [], True)
        TestFormat.validate_serialization(
            config_format,
            TEST_CASES,
            [],
            command,
        )

    def test_serialize_namespaced_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.ns_command_flags[0])
        config_format = CLIConfigFormat("test", [], False)
        TestFormat.validate_serialization(
            config_format,
            TEST_CASES,
            NAMESPACES,
            command,
        )

    def test_serialize_namespaced_short_flag_values(self):
        command = TestCLI._build_command(TEST_CASES, lambda t: t.ns_command_flags[1])
        config_format = CLIConfigFormat("test", [], True)
        TestFormat.validate_serialization(
            config_format,
            TEST_CASES,
            NAMESPACES,
            command,
        )
