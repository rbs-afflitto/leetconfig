PYTHON=python3
PIP=pip3

develop:
	$(PIP) install -e .[test]

install:
	$(PIP) install .

install-test:
	$(PIP) install .[test]

test:
	$(PYTHON) -m pytest --cov=leetconfig --junitxml=leetconfig_report.xml

python3-%: PYTHON=python3
python3-%: PIP=pip3
python3-develop: develop
python3-install: install
python3-install-test: install-test
python3-test: test

check-mypy:
	$(PYTHON) -m mypy
